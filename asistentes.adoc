== ﻿Lista de asistentes

// Ordenados por orden alfabético
// Formato: Apellidos, Nombre

* Luque Giráldez, José Rafael
* Martínez Bernal, Manuel
* Vega Zambrana, Adrián
* Viegas Peñalosa, Pedro

=== Organización del transporte

// Si tienes vehículo, pon el número de plazas. Si no tienes, añádete a
// alguno de los vehículos existentes.

==== Peugeot 508 (5 plazas)

* Rafael Luque
* Manuel Martínez
* Adrián Vega

==== Triciclo 2007 (1 plaza)

* Pedro Viegas
